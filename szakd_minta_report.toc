\select@language {magyar} \contentsline {chapter}{\numberline {1}Bevezet\'es}{7}%
\select@language {magyar} \contentsline {section}{\numberline {1.1}C\'elkit\H {u}z\'es}{7}%
\select@language {magyar} \contentsline {section}{\numberline {1.2}Motiv\'aci\'o}{7}%
\select@language {magyar} \contentsline {chapter}{\numberline {2}Weboldal optimaliz\'al\'as}{9}%
\select@language {magyar} \contentsline {section}{\numberline {2.1}Optimaliz\'al\'as fejl\H od\'est\"ort\'enete}{9}%
\select@language {magyar} \contentsline {section}{\numberline {2.2}Webalkalmaz\'asok optimaliz\'al\'asa }{9}%
\select@language {magyar} \contentsline {section}{\numberline {2.3}Keres\H ooptimaliz\'al\'as }{10}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.3.1}H\'atr\'altat\'o t\'enyez\H ok}{10}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.3.2}Praktik\'ak a magasabb rangsorol\'as \'erdek\'eben}{11}%
\select@language {magyar} \contentsline {section}{\numberline {2.4}Teljes\IeC {\'\i }tm\'eny optimaliz\'al\'asa}{11}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.4.1}Sebess\'eg }{12}%
\select@language {magyar} \contentsline {section}{\numberline {2.5}Sebess\'eg optimaliz\'al\'as }{13}%
\select@language {magyar} \contentsline {subsection}{\numberline {2.5.1}Teljes\IeC {\'\i }tm\'eny n\"ovel\H o praktik\'ak}{13}%
\select@language {magyar} \contentsline {section}{\numberline {2.6}Megval\'os\IeC {\'\i }that\'os\'ag, konkl\'uzi\'o}{15}%
\select@language {magyar} \contentsline {chapter}{\numberline {3}Fejleszt\H oi dokument\'aci\'o}{16}%
\select@language {magyar} \contentsline {section}{\numberline {3.1}Koncepci\'o, el\H ok\'esz\"uletek}{16}%
\select@language {magyar} \contentsline {section}{\numberline {3.2}Programspecifik\'aci\'o}{17}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.2.1}Adatfeldolgoz\'as}{17}%
\select@language {magyar} \contentsline {subsubsection}{Bemen\H o adatok}{17}%
\select@language {magyar} \contentsline {subsubsection}{Kimen\H o adatok}{18}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.2.2}K\'eperny\H otervek}{18}%
\select@language {magyar} \contentsline {subsubsection}{Log\'o}{18}%
\select@language {magyar} \contentsline {subsubsection}{Tervezett oldalak, s\'em\'ak}{19}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.2.3}Adatszerkezetek}{21}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.2.4}Tervezett funkci\'ok}{22}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.2.5}Hardver k\"ovetelm\'enyek}{24}%
\select@language {magyar} \contentsline {section}{\numberline {3.3}Fejleszt\H oeszk\"oz\"ok, k\"ornyezet kiv\'alaszt\'asa}{25}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.1}Kliens oldali lehet\H os\'egek}{25}%
\select@language {magyar} \contentsline {subsubsection}{TypeScript}{25}%
\select@language {magyar} \contentsline {subsubsection}{Keretrendszerek}{26}%
\select@language {magyar} \contentsline {subsubsection}{React.js}{26}%
\select@language {magyar} \contentsline {subsubsection}{Vue.js}{27}%
\select@language {magyar} \contentsline {subsubsection}{Angular/Angular.js}{27}%
\select@language {magyar} \contentsline {subsubsection}{Fejleszt\H o k\"ornyezetek}{27}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.2}Szerver oldali lehet\H os\'egek}{28}%
\select@language {magyar} \contentsline {subsubsection}{PHP alap\'u keretrendszerek}{28}%
\select@language {magyar} \contentsline {subsubsection}{Python alap\'u keretrendszerek}{28}%
\select@language {magyar} \contentsline {subsubsection}{C\# alap\'u keretrendszerek}{28}%
\select@language {magyar} \contentsline {subsubsection}{JAVA alap\'u keretrendszerek}{29}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.3}Adatb\'azis kezel\H o rendszerek}{29}%
\select@language {magyar} \contentsline {subsubsection}{MySQL}{29}%
\select@language {magyar} \contentsline {subsubsection}{MariaDB}{30}%
\select@language {magyar} \contentsline {subsubsection}{MongoDB}{30}%
\select@language {magyar} \contentsline {subsubsection}{Redis}{30}%
\select@language {magyar} \contentsline {subsubsection}{Oracle}{30}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.4}\"Osszegz\'es, v\'alasztott fejleszt\H oi eszk\"oz\"ok, k\"ornyezetek}{30}%
\select@language {magyar} \contentsline {subsubsection}{V\'alasztott fejleszt\H o eszk\"oz\"ok}{30}%
\select@language {magyar} \contentsline {subsubsection}{Fejleszt\H ok\"ornyezetek}{31}%
\select@language {magyar} \contentsline {subsubsection}{Egy\'eb haszn\'alni k\IeC {\'\i }v\'ant technol\'ogi\'ak}{32}%
\select@language {magyar} \contentsline {section}{\numberline {3.4}Terv v\'egleges\IeC {\'\i }t\'ese}{33}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.4.1}Tervezett modulok}{33}%
\select@language {magyar} \contentsline {subsubsection}{Szuperadmin}{33}%
\select@language {magyar} \contentsline {subsubsection}{C\'eg}{33}%
\select@language {magyar} \contentsline {subsubsection}{\"Ugyf\'el}{33}%
\select@language {magyar} \contentsline {subsubsection}{\"Uzlet}{34}%
\select@language {magyar} \contentsline {subsubsection}{Kassza}{34}%
\select@language {magyar} \contentsline {subsubsection}{Rakt\'ar}{34}%
\select@language {magyar} \contentsline {subsubsection}{Alapanyag}{34}%
\select@language {magyar} \contentsline {subsubsection}{Eszk\"oz}{34}%
\select@language {magyar} \contentsline {subsubsection}{Alapanyag/Eszk\"oz regisztr\'aci\'o}{34}%
\select@language {magyar} \contentsline {subsubsection}{Jelent\'es}{34}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.4.2}Modulok kapcsolata}{34}%
\select@language {magyar} \contentsline {subsubsection}{Kapcsolatok}{35}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.4.3}Jogosults\'agi szintekkel el\'erhet\H o funkci\'ok}{36}%
\select@language {magyar} \contentsline {subsubsection}{T\'aj\'ekoz\'od\'as, inform\'aci\'oszerz\'es}{36}%
\select@language {magyar} \contentsline {subsubsection}{Lek\'erdez\'es}{36}%
\select@language {magyar} \contentsline {subsubsection}{\'Uj elem felv\'etele}{37}%
\select@language {magyar} \contentsline {subsubsection}{Elemek m\'odos\IeC {\'\i }t\'asa}{37}%
\select@language {magyar} \contentsline {subsubsection}{Elemek t\"orl\'ese}{39}%
\select@language {magyar} \contentsline {subsubsection}{Statisztika megjelen\IeC {\'\i }t\'ese}{39}%
\select@language {magyar} \contentsline {subsubsection}{Rakt\'ari jelent\'es}{39}%
\select@language {magyar} \contentsline {subsubsection}{Z\'ar\'as jelent\'es}{39}%
\select@language {magyar} \contentsline {section}{\numberline {3.5}Backend fel\'ep\IeC {\'\i }t\'ese}{41}%
\select@language {magyar} \contentsline {subsubsection}{Rel\'aci\'os modellre val\'o lek\'epez\'es}{41}%
\select@language {magyar} \contentsline {subsubsection}{Hib\'ak elker\"ul\'ese}{43}%
\select@language {magyar} \contentsline {subsection}{\numberline {3.5.1}Probl\'em\'ak, egyedi megold\'asok}{43}%
\select@language {magyar} \contentsline {chapter}{\numberline {4}\"Osszefoglal\'as}{45}%
\select@language {magyar} \contentsline {chapter}{Irodalomjegyz\'ek}{46}%
\select@language {magyar} \contentsline {chapter}{Adathordoz\'o haszn\'alati \'utmutat\'o}{47}%
